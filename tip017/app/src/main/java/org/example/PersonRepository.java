package org.example;

import org.springframework.data.querydsl.QuerydslPredicateExecutor;

// import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends CrudRepository<Person, String>, QuerydslPredicateExecutor<Person> {
    //
}
