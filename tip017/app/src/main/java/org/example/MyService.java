package org.example;

import java.util.Date;
// import java.util.List;
// import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MyService{
    @Autowired
    PersonRepository repo;

	@Transactional
    public void doStart(){

        Person p = new Person();
  		p.name = "hoge";
		p.location = "japan";
		p.val = 246.8f;
		p.dateTime = new Date();
		//NG: Caused by: java.lang.IllegalArgumentException: Did not find a query class org.example.QPerson for domain class org.example.Person
		repo.save(p);
    }
}
