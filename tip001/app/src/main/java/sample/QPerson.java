package sample;

import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.sql.RelationalPathBase;
// import static com.querydsl.core.types.dsl.Expressions.*;

public class QPerson extends RelationalPathBase<QPerson> {

    private static final long serialVersionUID = 1L;

    public static final QPerson person = new QPerson("person");

    //  public final NumberPath<Long> id = createNumber("id", Long.class);
    //  public final StringPath firstName = createString("first_name");
    //  public final StringPath lastName = createString("last_name");
    public final StringPath name = createString("name");

    // public QPerson(String variable) {
    //     super(QPerson.class, forVariable(variable), "SCOTT", "person");
    // }
    public QPerson(String variable) {
        super(QPerson.class, variable, "SCOTT", "person");
        System.out.println("** variable: "+variable);
    }

    public QPerson(Path<? extends QPerson> path) {
        super(path.getType(), path.getMetadata(), "SCOTT", "person");
        System.out.println("** path: "+path);
    }

    public QPerson(PathMetadata metadata) {
        super(QPerson.class, metadata, "SCOTT", "person");
        System.out.println("** metadata: "+metadata);
    }

}