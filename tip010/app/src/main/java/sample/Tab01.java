package sample;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name="tab01")
public class Tab01 implements Serializable {
    private int    num01;
    private String chr01;
    private Date   dat01;
}

