# めも

### Oracle

- docker login registry.gitlab.com
- docker pull registry.gitlab.com/tomate-broccoli/gitpod_tip011 
- docker run -d --env-file ../oracle.env --name ORCL2 -p 1521:1521 registry.gitlab.com/tomate-broccoli/gitpod_tip011
- docker exec -it ORCL2 sqlplus system/oracle21C3@MYORCL

### テーブル

```
create user scott identified by tiger account unlock;
grant connect, resource to scott ;
alter user scott quota UNLIMITED on users ;
exit ;

docker exec -it ORCL2 sqlplus scott/tiger@MYORCL

create table tab01 (
    num01 number(10),
    chr01 varchar2(40),
    dat01 date
);
insert into tab01 values(1, 'abcde', sysdate);
insert into tab01 values(2, 'vwxyz', sysdate+1);
commit;
select * from tab01 ;
```
